import { scene } from "../three";
import { svgManager, imageManager } from "../three/utils";
import {
  animationManager,
  Timeline,
  CAMERA_ANIMATION_DURATION
} from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await imageManager.bulkGet(["kohol.kohol-illus", "kohol.kohol-sculpt"]);
  await Promise.all([
    imageManager.get("kohol.kohol-illus").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = 7.25;
      elm.position.y = -1;
      elm.position.z = -31;
      scene.add(elm);
    }),
    imageManager.get("kohol.kohol-sculpt").then(elm => {
      elm.scale.set(1.45, 1.45, 1.45);
      elm.position.x = 7.75;
      elm.position.y = -1.2;
      elm.position.z = -30.75;
      scene.add(elm);
    }),
    imageManager.getAnimated("kohol.fleurs_anime", 7, 7, 44, 200).then(elm => {
      elm.scale.set(0.3, 0.3, 0.3);
      elm.position.x = 6.4;
      elm.position.y = -0.5;
      elm.position.z = -31;
      // animationManager.add("intro-illu-droite", elm);
      scene.add(elm);
    })
  ]);
  timeline.add(
    animationManager
      .get("camera")
      .animate("position", CAMERA_ANIMATION_DURATION, {
        x: 6.75,
        y: -1,
        z: -29.5
      }),
    0
  );
  timeline.show(document.querySelector(".home"), 0);
  animationManager.setSectionTimeline("home", timeline.get());
}
