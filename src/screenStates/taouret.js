import { scene } from "../three";
import { svgManager, imageManager } from "../three/utils";
import {
  animationManager,
  Timeline,
  CAMERA_ANIMATION_DURATION
} from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await imageManager.bulkGet([
    "taouret.taouretbes-illus",
    "taouret.taouretbes-sculpt"
  ]);
  await Promise.all([
    imageManager.get("taouret.taouretbes-illus").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = 2.25;
      elm.position.y = -2.5;
      elm.position.z = -31;
      scene.add(elm);
    }),
    imageManager.get("taouret.taouretbes-sculpt").then(elm => {
      elm.scale.set(1.6, 1.6, 1.6);
      elm.position.x = 2.85;
      elm.position.y = -2.6;
      elm.position.z = -31;
      scene.add(elm);
    }),
    imageManager.getAnimated("taouret.chat_anime", 7, 7, 49, 200).then(elm => {
      elm.scale.set(0.11, 0.11, 0.11);
      elm.position.x = 2.11;
      elm.position.y = -2.18;
      elm.position.z = -30.75;
      scene.add(elm);
    }),
    imageManager.getAnimated("taouret.coeurs_anime", 3, 3, 9, 200).then(elm => {
      elm.scale.set(0.15, 0.15, 0.15);
      elm.position.x = 2.72;
      elm.position.y = -2;
      elm.position.z = -30.75;
      scene.add(elm);
    })
  ]);

  timeline.add(
    animationManager
      .get("camera")
      .animate("position", CAMERA_ANIMATION_DURATION, {
        x: 2,
        y: -2.5,
        z: -29.5
      }),
    0
  );
  timeline.show(document.querySelector(".spouse"), 0);
  animationManager.setSectionTimeline("spouse", timeline.get());
}
