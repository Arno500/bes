import { scene } from "../three";
import { imageManager } from "../three/utils";
import {
  animationManager,
  Timeline,
  CAMERA_ANIMATION_DURATION
} from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await imageManager.bulkGet([
    "facebes.facebes-illus",
    "facebes.facebes-sculpt"
  ]);
  await Promise.all([
    imageManager.get("facebes.facebes-illus").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = 1.25;
      elm.position.y = 2;
      elm.position.z = -31;
      scene.add(elm);
    }),
    imageManager.get("facebes.facebes-sculpt").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = 1.25;
      elm.position.y = 1.8;
      elm.position.z = -30.75;
      scene.add(elm);
    }),
    imageManager
      .getAnimated("facebes.sparkles_anime", 5, 5, 24, 200)
      .then(elm => {
        elm.scale.set(0.35, 0.35, 0.35);
        elm.position.x = 1.25;
        elm.position.y = 2;
        elm.position.z = -30.5;
        // animationManager.add("intro-illu-droite", elm);
        scene.add(elm);
      })
  ]);
  timeline.add(
    animationManager
      .get("camera")
      .animate("position", CAMERA_ANIMATION_DURATION, {
        x: 1,
        y: 2,
        z: -29.5
            }),
    0
  );
  timeline.show(document.querySelector(".gods"), 0);
  animationManager.setSectionTimeline("gods", timeline.get());
}
