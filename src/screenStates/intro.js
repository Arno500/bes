import { scene } from "../three";
import { svgManager, imageManager } from "../three/utils";
import { animationManager, Timeline } from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await svgManager.bulkGet(["nuage_clair", "nuage_fonce"]);

  await Promise.all([
    // TODO: TEST FOR ANIMATED ELEMENT
    // imageManager.getAnimated("kohol.fleurs_anime", 7, 7, 44, 200).then(elm => {
    //   // elm.scale.set(1.5, 1.5, 1.5);
    //   elm.position.x = 0;
    //   elm.position.y = 0;
    //   elm.position.z = 2;
    //   // animationManager.add("intro-illu-droite", elm);
    //   scene.add(elm);
    // }),
    // Nuages
    svgManager.get("nuage_fonce").then(elm => {
      elm.scale.multiplyScalar(0.75);
      elm.position.x = -2.5;
      elm.position.y = -2.25;
      elm.position.z = 1;
      animationManager.add("nuage1", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_clair").then(elm => {
      elm.scale.multiplyScalar(1.5);
      elm.position.x = 8;
      elm.position.y = -2;
      elm.position.z = -1;
      animationManager.add("nuage2", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_clair").then(elm => {
      elm.scale.multiplyScalar(1.75);
      elm.position.x = -3;
      elm.position.y = -0.5;
      elm.position.z = -3;
      animationManager.add("nuage3", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_fonce").then(elm => {
      elm.scale.multiplyScalar(1.2);
      elm.position.x = 9;
      elm.position.y = -0.7;
      elm.position.z = -5;
      animationManager.add("nuage4", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_clair").then(elm => {
      elm.scale.multiplyScalar(3.5);
      elm.position.x = 13;
      elm.position.y = 5;
      elm.position.z = -7;
      animationManager.add("nuage5", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_fonce").then(elm => {
      elm.scale.multiplyScalar(1.5);
      elm.position.x = -15;
      elm.position.y = 3;
      elm.position.z = -9;
      animationManager.add("nuage6", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_clair").then(elm => {
      elm.scale.multiplyScalar(3.5);
      elm.position.x = -8;
      elm.position.y = 9;
      elm.position.z = -11;
      animationManager.add("nuage7", elm);
      scene.add(elm);
    }),
    svgManager.get("nuage_fonce").then(elm => {
      elm.scale.multiplyScalar(1.5);
      elm.position.x = -1;
      elm.position.y = 12;
      elm.position.z = -12;
      animationManager.add("nuage8", elm);
      scene.add(elm);
    })
  ]).then(() => {
    timeline.add(
      animationManager.get("nuage1").animate("position", 1, { x: -4.5 }),
      0
    );
    timeline.add(
      animationManager.get("nuage2").animate("position", 1, { x: 11 }),
      0
    );
    timeline.add(
      animationManager.get("nuage3").animate("position", 1, { x: -5 }),
      0
    );
    timeline.add(
      animationManager.get("nuage4").animate("position", 1, { x: 12 }),
      0
    );
    timeline.add(
      animationManager.get("nuage5").animate("position", 1, { x: 16 }),
      0
    );
    timeline.add(
      animationManager.get("nuage6").animate("position", 1, { x: -20 }),
      0
    );
    timeline.add(
      animationManager.get("nuage7").animate("position", 1, { x: -11 }),
      0
    );
    timeline.add(
      animationManager.get("nuage8").animate("position", 1, { x: -4 }),
      0
    );
    timeline.add(
      animationManager
        .get("camera")
        .animate("position", 1, { z: 2 }, 0, "power2.inOut"),
      0
    );
    timeline.hide(document.querySelector(".intro"), 0);
    timeline.hide(document.querySelector(".img-container"), 0);
    timeline.show(document.querySelector(".intro2"), 0);
    animationManager.addTimeline(timeline.get(), "intro");
  });
}
