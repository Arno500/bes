import { scene } from "../three";
import {
  svgManager,
  imageManager,
  CAMERA_ANIMATION_DURATION,
  CAMERA_MAP_VIEW_Z
} from "../three/utils";
import { animationManager, Timeline } from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await imageManager.bulkGet(["carte.carto-illus", "carte.carto-sculpt"]);
  await Promise.all([
    imageManager.get("carte.carto-illus").then(elm => {
      elm.scale.set(32, 32, 32);
      elm.position.x = 0;
      elm.position.y = 0;
      elm.position.z = -29;
      scene.add(elm);
    }),
    imageManager.get("carte.carto-sculpt").then(elm => {
      elm.scale.set(20, 20, 20);
      elm.position.x = 10;
      elm.position.y = 0;
      elm.position.z = -25;
      scene.add(elm);
    })
  ]);
  timeline.add(
    animationManager
      .get("camera")
      .animate("position", CAMERA_ANIMATION_DURATION, { z: CAMERA_MAP_VIEW_Z }),
    0
  );
  animationManager.addTimeline(timeline.get(), "map");
}
