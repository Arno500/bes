import { svgManager } from "../three/utils";
import { animationManager, Timeline } from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await svgManager.bulkGet(["nuage_clair", "nuage_fonce"]);
  await Promise.all([
    // Illus autour TODO : à migrer vres le DOM pour réutilisation
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(2);
    //   elm.position.x = -10;
    //   elm.position.y = 4;
    //   elm.position.z = -23;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_fonce").then(elm => {
    //   elm.scale.multiplyScalar(2);
    //   elm.position.x = 9;
    //   elm.position.y = -2;
    //   elm.position.z = -15;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(2);
    //   elm.position.x = 10;
    //   elm.position.y = 6;
    //   elm.position.z = -4;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_fonce").then(elm => {
    //   elm.scale.multiplyScalar(3);
    //   elm.position.x = -18;
    //   elm.position.y = -5;
    //   elm.position.z = -18;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(3);
    //   elm.position.x = -12;
    //   elm.position.y = 2;
    //   elm.position.z = -23;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_fonce").then(elm => {
    //   elm.scale.multiplyScalar(4);
    //   elm.position.x = -4;
    //   elm.position.y = -1;
    //   elm.position.z = -27;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(3);
    //   elm.position.x = 0;
    //   elm.position.y = 16;
    //   elm.position.z = -27;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_fonce").then(elm => {
    //   elm.scale.multiplyScalar(1);
    //   elm.position.x = -6;
    //   elm.position.y = -3.5;
    //   elm.position.z = -4;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(1);
    //   elm.position.x = 4;
    //   elm.position.y = -0.8;
    //   elm.position.z = 2;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_fonce").then(elm => {
    //   elm.scale.multiplyScalar(1);
    //   elm.position.x = -3;
    //   elm.position.y = 0.85;
    //   elm.position.z = 2;
    //   scene.add(elm);
    // }),
    // svgManager.get("nuage_clair").then(elm => {
    //   elm.scale.multiplyScalar(1.5);
    //   elm.position.x = 1;
    //   elm.position.y = 1;
    //   elm.position.z = 2;
    //   scene.add(elm);
    // })
  ]);
  timeline.add(
    animationManager.get("nuage1").animate("position", 1, { x: -6 }),
    0
  );
  timeline.add(
    animationManager.get("nuage2").animate("position", 1, { x: 13 }),
    0
  );
  timeline.add(
    animationManager.get("nuage3").animate("position", 1, { x: -7 }),
    0
  );
  timeline.add(
    animationManager.get("nuage4").animate("position", 1, { x: 15 }),
    0
  );
  timeline.add(
    animationManager.get("nuage5").animate("position", 1, { x: 20 }),
    0
  );
  timeline.add(
    animationManager.get("nuage6").animate("position", 1, { x: -25 }),
    0
  );
  timeline.add(
    animationManager.get("nuage7").animate("position", 1, { x: -13 }),
    0
  );
  timeline.add(
    animationManager.get("nuage8").animate("position", 1, { x: -6 }),
    0
  );
  timeline.add(
    animationManager.get("camera").animate("position", 1, { z: -10 }, 0),
    0
  );
  timeline.add(
    animationManager.get("fog").animate(1, { near: 25, far: 35 }, 0),
    0
  );
  timeline.hide(document.querySelector(".intro2"), 0);
  timeline.show(document.querySelector(".map"), 0);
  animationManager.addTimeline(timeline.get(), "intro2");
}
