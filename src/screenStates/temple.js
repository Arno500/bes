import { scene } from "../three";
import { svgManager, imageManager } from "../three/utils";
import {
  animationManager,
  Timeline,
  CAMERA_ANIMATION_DURATION,
  CAMERA_MAP_VIEW_Z
} from "../three/animation";

const timeline = new Timeline();

export default async function setupScene() {
  await imageManager.bulkGet(["temple.temple-illus", "temple.temple-sculpt"]);
  await Promise.all([
    imageManager.get("temple.temple-illus").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = -3.75;
      elm.position.y = 1;
      elm.position.z = -31;
      scene.add(elm);
    }),
    imageManager.get("temple.temple-sculpt").then(elm => {
      elm.scale.set(1.5, 1.5, 1.5);
      elm.position.x = -3.25;
      elm.position.y = 0.8;
      elm.position.z = -30.75;
      scene.add(elm);
    }),
    imageManager.getAnimated("temple.feu_anime", 4, 4, 16, 200).then(elm => {
      elm.scale.set(0.35, 0.35, 0.35);
      elm.position.x = -3.775;
      elm.position.y = 1;
      elm.position.z = -31;
      // animationManager.add("intro-illu-droite", elm);
      scene.add(elm);
    })
  ]);
  timeline.add(
    animationManager
      .get("camera")
      .animate("position", CAMERA_ANIMATION_DURATION, {
        x: -4,
        y: 1,
        z: -29.5
      }),
    0
  );
  timeline.show(document.querySelector(".temple"), 0);
  animationManager.setSectionTimeline("temple", timeline.get());
}
