import setupIntro from "./intro";
import setupMap from "./map";
import setupTemple from "./temple";
import setupKohol from "./kohol";
import setupFaceBes from "./faceBes";
import setupTaouret from "./taouret";
import setupClouds from "./clouds";
import { animationManager } from "../three/animation";

async function setupScenes() {
  await setupIntro();
  await setupClouds();
  await setupMap();
  await setupTemple();
  await setupKohol();
  await setupFaceBes();
  await setupTaouret();
}

setupScenes();
