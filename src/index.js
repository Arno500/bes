import "./styles.scss";
import "../modernizr";
import gsap from "gsap";
import { rafManager } from "./global";
import "./three";
import "./events";
import "./screenStates";
import webAudioTouchUnlock from "web-audio-touch-unlock";

export const context = new (window.AudioContext || window.webkitAudioContext)();
const DEFAULT_BG_VOLUME = 0.1;
const bgMusic = document.getElementById("bg-musique");
bgMusic.volume = DEFAULT_BG_VOLUME;
bgMusic.play();

webAudioTouchUnlock(context).then(
  function() {
    bgMusic.play();
  },
  function(reason) {
    console.error(reason);
  }
);

document.querySelector(".start").addEventListener("click", () => {
  if (bgMusic.paused === true) {
    bgMusic.volume = 0;
    bgMusic.play();
    gsap.to(bgMusic, { volume: DEFAULT_BG_VOLUME, duration: 1 });
  }
});

// var source = context.createBufferSource();
// source.buffer = buffer;
// source.connect(context.destination);
// source.start();

rafManager.start();
