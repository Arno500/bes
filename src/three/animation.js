import { gsap } from "gsap";
import CustomEase from "gsap/CustomEase";
import { getImageLink } from "./utils";
import eachDeep from "deepdash/eachDeep";

gsap.registerPlugin(CustomEase);

export const CAMERA_ANIMATION_DURATION = 2;
export const CAMERA_MAP_VIEW_Z = -32;

class AnimatedElement {
  constructor(element) {
    this.element = element;
    this.state = 0;
  }

  animate(property, duration, destination, position = 0) {
    if (!Array.isArray(property)) {
      return gsap.to(
        this.element[property],
        { duration, ease: "power2.inOut", ...destination },
        position
      );
    } else {
      return property.map(elm => this.animate(...elm));
    }
  }
}

class AnimatedFogElement extends AnimatedElement {
  constructor(element) {
    super(element);
  }
  animate(duration, destination, position = 0) {
    return gsap.to(
      this.element,
      { duration, ease: "power2.inOut", ...destination },
      position
    );
  }
}
class AnimatedCameraElement extends AnimatedElement {
  constructor(element) {
    super(element);
  }
  /**
   *
   * @param {String} property Property to animate (often "position")
   * @param {Number} duration Duration in seconds
   * @param {Object} destination Destination properties, can also include custom Tween params
   * @param {Number} position Position of the animation in the timeline (defaulting to 0, launching everything together)
   * @param {String|Object} ease Ease function from GSAP, or processed CustomEase
   */
  animate(
    property,
    duration,
    destination,
    position = 0,
    ease = CustomEase.create(
      "custom",
      "M0,0 C0,0 0.06704,0.00079 0.10758,0.00611 0.14172,0.01059 0.16424,0.01492 0.19617,0.02573 0.22616,0.03587 0.24655,0.04543 0.27322,0.06214 0.29886,0.07821 0.31538,0.092 0.3368,0.11386 0.36171,0.13928 0.37628,0.15807 0.39665,0.18807 0.41967,0.22197 0.43391,0.24364 0.45057,0.28138 0.51793,0.43395 0.54655,0.53223 0.61581,0.68448 0.64554,0.74984 0.6694,0.7893 0.70796,0.84724 0.72686,0.87565 0.74406,0.89259 0.76943,0.91531 0.79188,0.93542 0.80949,0.94805 0.83593,0.96187 0.86291,0.97597 0.88387,0.98374 0.91361,0.99066 0.9448,0.99791 1,1 1,1 "
    )
  ) {
    let arrayOfTweens = [];
    if (typeof destination.x !== "undefined") {
      arrayOfTweens.push(
        gsap.to(
          this.element[property],
          {
            duration,
            ease: "power1.inOut",
            onUpdate: () => this.element.updateProjectionMatrix(),
            x: destination.x
          },
          position
        )
      );
    }
    if (typeof destination.y !== "undefined") {
      arrayOfTweens.push(
        gsap.to(
          this.element[property],
          {
            duration,
            ease: "power1.inOut",
            onUpdate: () => this.element.updateProjectionMatrix(),
            y: destination.y
          },
          position
        )
      );
    }
    if (typeof destination.z !== "undefined") {
      arrayOfTweens.push(
        gsap.to(
          this.element[property],
          {
            duration,
            ease,
            onUpdate: () => this.element.updateProjectionMatrix(),
            ...destination,
            z: destination.z
          },
          position
        )
      );
    }
    return arrayOfTweens;
  }
}

class AnimationManager {
  constructor() {
    this.elements = new Map();
    this.globalTimeline = gsap.timeline({ paused: true });
    gsap.globalTimeline.smoothChildTiming = false;
    window.testTimeline = this.globalTimeline;
    this.tweening = null;
    this.currentSection = null;
    this.sectionTimelines = new Map();
    this.mapTween = null;
    this.sectionsManager = null;
  }

  /**
   *
   * @param {String} elm
   * @returns {AnimatedElement|AnimatedCameraElement}
   */
  get(elm) {
    if (!this.elements.has(elm))
      throw new Error(
        "Element " +
          elm +
          " was not found in animationManager. You probably forgot to add it, or it was a bit early ?"
      );
    return this.elements.get(elm);
  }

  add(key, elm) {
    this.elements.set(key, new AnimatedElement(elm));
  }

  addCamera(key, elm) {
    this.elements.set(key, new AnimatedCameraElement(elm));
  }

  addFog(key, elm) {
    this.elements.set(key, new AnimatedFogElement(elm));
  }

  addTimeline(timeline, label) {
    // this.globalTimeline.addLabel(label);
    this.globalTimeline.add(timeline, label);
    // this.globalTimeline.addLabel(label + "_end");
  }

  goTo(label) {
    return new Promise((resolve, reject) => {
      if (!this.isTweening()) {
        this.currentSection = label;
        this.tweening = this.globalTimeline.tweenTo(label, {
          onComplete: () => {
            resolve();
            this.tweening = null;
          }
        });
      } else {
        reject("Tweening in progress, please wait or use a promise");
      }
    });
  }

  isTweening() {
    return this.tweening != null;
  }

  goToSection(section) {
    if (!this.isTweening()) {
      console.log("Going to " + section);
      window.currentTimeline = this.sectionTimelines.get(section);
      this.tweening = this.sectionTimelines.get(section).tweenTo("+=0", {
        onComplete: () => {
          this.tweening = null;
        }
      });
      this.currentSection = section;
      this.hideSelectionScreen();
    }
  }

  next() {
    this.goTo(this.globalTimeline.nextLabel());
  }

  hideSelectionScreen() {
    document
      .querySelectorAll("section")
      .forEach(elm => elm.classList.add("hidden"));
  }

  selectionScreen() {
    document.querySelectorAll("section").forEach(elm => {
      elm.classList.add("hidden");
      const sound = elm.querySelector("audio");
      if (sound != null) {
        sound.pause();
        sound.currentTime = 0;
      }
    });
    if (typeof this.sectionsManager.nextSections() !== "undefined") {
      setTimeout(
        () =>
          document.querySelector(".img-container").classList.remove("hidden"),
        0
      );
      this.sectionsManager.buildSelectionScreen();
      setTimeout(
        () =>
          document.querySelector(".selectionscreen").classList.remove("hidden"),
        0
      );
    } else {
      const outro = document.querySelector(".outro");
      outro.classList.remove("hidden");
      outro.querySelector("audio").play();
      setTimeout(
        () => outro.addEventListener("click", () => location.reload()),
        5000
      );
      setTimeout(() => location.reload(), 25000);
    }
  }

  goToMap() {
    return new Promise(resolve => {
      if (!this.isTweening()) {
        this.tweening = this.sectionTimelines
          .get(this.currentSection)
          .tweenTo("0", {
            onComplete: () => {
              this.tweening = null;
              this.currentSection = null;
              document.querySelectorAll("section").forEach(elm => {
                const sound = elm.querySelector("audio");
                if (sound != null) {
                  sound.pause();
                  sound.currentTime = 0;
                }
              });
            },
            onStart: () => {
              resolve();
            }
          });
      }
    });
  }

  setMapTween(tween) {
    this.mapTween = tween;
  }

  setSectionTimeline(section, tween) {
    this.sectionTimelines.set(section, tween);
  }
}

export class Timeline {
  constructor() {
    this.timeline = gsap.timeline();
    this.displayState = new Map();
  }
  add(elm, pos) {
    if (Array.isArray(elm)) {
      elm.forEach(anim => this.timeline.add(anim, pos));
    } else {
      this.timeline.add(elm, pos);
    }
  }
  hide(elm, pos = 0, delay = 0) {
    this.timeline.set(
      elm,
      {
        onStart: elm => {
          elm.classList.add("hidden");
          const sound = elm.querySelector("audio");
          if (sound !== null) {
            sound.pause();
            sound.currentTime = 0;
          }
        },
        onStartParams: [elm],
        onReverseCompleteParams: [elm],
        onReverseComplete: elm => {
          elm.classList.remove("hidden");
          const sound = elm.querySelector("audio");
          if (sound !== null) {
            sound.pause();
            sound.currentTime = 0;
          }
        },
        delay
      },
      pos
    );
  }
  show(elm, pos = 0, delay = 0) {
    this.displayState.set(elm, false);
    this.timeline.set(
      elm,
      {
        onStart: elm => {
          if (this.displayState.get(elm) === false) {
            this.displayState.set(elm, true);
            elm.classList.remove("hidden");
            const sound = elm.querySelector("audio");
            if (sound !== null) {
              this.audioTimeout = setTimeout(() => sound.play(), 1000);
            }
          }
        },
        onStartParams: [elm],
        onReverseCompleteParams: [elm],
        onReverseComplete: elm => {
          elm.classList.add("hidden");
          const sound = elm.querySelector("audio");
          if (sound !== null) {
            clearTimeout(this.audioTimeout);
            sound.pause();
            sound.currentTime = 0;
          }
        },
        delay
      },
      pos
    );
  }
  get() {
    return this.timeline;
  }
}

class SectionsManager {
  constructor() {
    this.sections = {
      name: "root",
      children: [
        {
          name: "temple",
          text: "Aller au<br>temple",
          img: "select.temple",
          children: [
            {
              name: "gods",
              text: "Voir les autres dieux",
              img: "select.gods",
              children: [
                {
                  name: "home",
                  text: "Protéger un foyer",
                  img: "select.home",
                  children: [
                    {
                      name: "spouse",
                      text: "Rencontrer mon épouse",
                      img: "select.spouse"
                    },
                    {
                      name: "dummy",
                      text: "Explorer mon temple à Bahariya",
                      img: "select.bahariya"
                    }
                  ]
                },
                {
                  name: "dummy",
                  text: "Encourager la fertilité",
                  img: "select.fertility"
                }
              ]
            },
            {
              name: "dummy",
              text: "Rencontrer la déesse lointaine",
              img: "select.goddess"
            }
          ]
        },
        {
          name: "dummy",
          text: "Découvrir les anciens empires égyptiens",
          img: "select.empires"
        }
      ]
    };
    this.currentSection = "root";
    this.path = [];
    this.selectionScreen = document.querySelector(".selectionscreen .flex");
  }

  getSection(section) {
    let result;
    eachDeep(
      this.sections,
      elm => {
        if (elm.name === section) result = elm;
      },
      { childrenPath: "children" }
    );
    return result;
  }

  nextSections() {
    return this.getSection(this.currentSection).children;
  }

  cleanSections() {
    while (this.selectionScreen.firstChild) {
      this.selectionScreen.removeChild(this.selectionScreen.firstChild);
    }
  }

  buildSelectionScreen() {
    let children = this.nextSections();
    this.cleanSections();
    children.forEach(elm =>
      this.selectionScreen.insertAdjacentHTML(
        "beforeend",
        `<div class="goToDestination" data-destination="${elm.name}">
              <img src="${elm.img ? getImageLink(elm.img) : ""}" alt="" />
            <span>${elm.text}</span>
          </div>`
      )
    );
    document.querySelectorAll(".goToDestination").forEach(elm => {
      elm.addEventListener("click", evt => {
        evt.preventDefault();
        const newSection = evt.currentTarget.getAttribute("data-destination");
        try {
          animationManager.goToSection(newSection);
        } catch (err) {
          console.error("Section probably doesn't exists: " + newSection, err);
        }
        this.currentSection = newSection;
        this.path.push(newSection);
      });
    });
  }
}

export const animationManager = new AnimationManager();
export const sectionsManager = new SectionsManager();
sectionsManager.nextSections();
animationManager.sectionsManager = sectionsManager;
