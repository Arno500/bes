// import Stats from "stats.js";
import * as THREE from "three";
import { rafManager, resizeManager } from "../global";
import { animationManager } from "./animation";
import { imageManager } from "./utils";

export const scene = new THREE.Scene();
scene.fog = new THREE.Fog(0xe7eaf3, 15, 20);
animationManager.addFog("fog", scene.fog);

// Debugging for Three.JS inspector
window.THREE = THREE;
window.scene = scene;

// Performance profiling
// const stats = new Stats();
// document.body.appendChild(stats.dom);
// stats.dom.style.position = "absolute";

const clock = new THREE.Clock();
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  1,
  1000
);
animationManager.addCamera("camera", camera);

const renderer = new THREE.WebGLRenderer({ antialias: false, alpha: false });
renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1);
scene.background = new THREE.Color(0xe7eaf3);

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

scene.add(camera);

camera.position.z = 5;
var animate = () => {
  // stats.update();
  renderer.render(scene, camera);
  imageManager.updateAnimatedTexture(1000 * clock.getDelta());
};
rafManager.addFunction(animate);

animate();

const resize = function(e) {
  renderer.setSize(e.target.innerWidth, e.target.innerHeight);
  camera.aspect = e.target.innerWidth / e.target.innerHeight;
  camera.updateProjectionMatrix();
};
resizeManager.addFunction(resize);
