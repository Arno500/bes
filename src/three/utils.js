import * as THREE from "three";
import { SVGLoader } from "three/examples/jsm/loaders/SVGLoader";
import lodashGet from "lodash/get";
import SVGs from "../svgs/*.svg";
import imgs from "../res/**";

class TextureAnimator {
  constructor(texture, tilesHoriz, tilesVert, numTiles, tileDispDuration) {
    this.texture = texture;
    this.tilesHorizontal = tilesHoriz;
    this.tilesVertical = tilesVert;
    // how many images does this spritesheet contain?
    //  usually equals tilesHoriz * tilesVert, but not necessarily,
    //  if there at blank tiles at the bottom of the spritesheet.
    this.numberOfTiles = numTiles;
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(1 / this.tilesHorizontal, 1 / this.tilesVertical);
    // how long should each image be displayed?
    this.tileDisplayDuration = tileDispDuration;
    // how long has the current image been displayed?
    this.currentDisplayTime = 0;
    // which image is currently being displayed?
    this.currentTile = 0;

    this.mesh = null;
  }
  update(milliSec) {
    this.currentDisplayTime += milliSec;
    while (this.currentDisplayTime > this.tileDisplayDuration) {
      this.currentDisplayTime -= this.tileDisplayDuration;
      this.currentTile++;
      if (this.currentTile == this.numberOfTiles) this.currentTile = 0;
      const currentColumn = this.currentTile % this.tilesHorizontal;
      this.texture.offset.x = currentColumn / this.tilesHorizontal;
      const currentRow = Math.floor(this.currentTile / this.tilesHorizontal);
      this.texture.offset.y =
        1 - currentRow / this.tilesVertical - 1 / this.tilesVertical;
    }
  }
}

/**
 *
 * @param {*} url The URL to the SVG
 * @returns {Promise} A promise, with a ThreeJS group of elements, ready to be integrated to a scene
 */
function loadSVG(url) {
  return new Promise((resolve, reject) => {
    const loader = new SVGLoader();
    loader.load(
      url,
      function(data) {
        const paths = data.paths;
        const group = new THREE.Group();
        group.scale.multiplyScalar(0.01);
        group.scale.y *= -1;
        let path;
        let fillColor;
        let strokeColor;
        let material;
        let geometry;
        let mesh;
        for (var i = 0; i < paths.length; i++) {
          path = paths[i];
          fillColor = path.userData.style.fill;
          if (fillColor !== undefined && fillColor !== "none") {
            material = new THREE.MeshBasicMaterial({
              color: new THREE.Color().setStyle(fillColor),
              opacity: path.userData.style.fillOpacity,
              transparent: path.userData.style.fillOpacity < 1,
              side: THREE.FrontSide,
              depthWrite: true
            });
            let shapes = path.toShapes(true);
            for (let j = 0; j < shapes.length; j++) {
              let shape = shapes[j];
              geometry = new THREE.ShapeBufferGeometry(shape, 6);
              geometry.center();
              mesh = new THREE.Mesh(geometry, material);
              group.add(mesh);
            }
          }
          strokeColor = path.userData.style.stroke;
          if (strokeColor !== undefined && strokeColor !== "none") {
            material = new THREE.MeshBasicMaterial({
              color: new THREE.Color().setStyle(strokeColor),
              opacity: path.userData.style.strokeOpacity,
              transparent: path.userData.style.strokeOpacity < 1,
              side: THREE.FrontSide,
              depthWrite: true
            });
            for (let j = 0, jl = path.subPaths.length; j < jl; j++) {
              let subPath = path.subPaths[j];
              geometry = SVGLoader.pointsToStroke(
                subPath.getPoints(),
                path.userData.style
              );
              if (geometry) {
                geometry.center();
                mesh = new THREE.Mesh(geometry, material);
                group.add(mesh);
              }
            }
          }
        }
        resolve(group);
      },
      null,
      () => {
        reject();
      }
    );
  });
}

class SVGManager {
  constructor() {
    this.loaded = new Map();
  }
  get(svgName) {
    return new Promise(resolve => {
      if (this.loaded.has(svgName)) {
        resolve(this.loaded.get(svgName).clone());
      } else {
        if (typeof SVGs[svgName] === "undefined")
          throw new Error("SVG not found: " + svgName);
        loadSVG(SVGs[svgName])
          .then(group => {
            group.name = svgName;
            this.loaded.set(svgName, group);
            group = group.clone();
            resolve(group);
          })
          .catch(() => {
            throw new Error("Could not load " + svgName);
          });
      }
    });
  }
  bulkGet(array) {
    let output = new Map();
    let promiseArray = array.map(elm => {
      return new Promise(resolve => {
        this.get(elm).then(group => {
          output.set(elm, group);
          resolve(group);
        });
      });
    });
    return Promise.all(promiseArray).then(() => {
      output.forEach((elm, key) => {
        output.set(key, elm.clone());
      });
      return output;
    });
  }
}

export function getImageLink(img) {
  const splitted = img.split(".");
  let folder = splitted[0];
  let name = splitted[1];
  if (window.Modernizr.webp) {
    name = name + ".webp";
  } else {
    name = name + ".png";
  }
  return lodashGet(imgs, [folder, name]);
}

function loadImgTexture(imgName) {
  return new Promise((resolve, reject) => {
    new THREE.TextureLoader().load(
      getImageLink(imgName),
      texture => resolve(texture),
      null,
      error => reject(error)
    );
  });
}

function loadImg(imgName, animated = false, ...animationParams) {
  return new Promise((resolve, reject) => {
    loadImgTexture(imgName, animated)
      .then(map => {
        let animatedTexture;
        if (animated === true) {
          animatedTexture = new TextureAnimator(map, ...animationParams);
          map = animatedTexture.texture;
        }
        map.minFilter = THREE.LinearFilter;
        const material = new THREE.MeshBasicMaterial({
          map,
          transparent: true,
          depthWrite: true
        });
        const geometry = new THREE.PlaneBufferGeometry(
          map.image.width / 1000,
          map.image.height / 1000
        );
        geometry.center();
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(0, 0, 0);
        resolve({ mesh, animatedTexture });
      })
      .catch(reject);
  });
}

class ImageManager {
  constructor() {
    this.loaded = new Map();
    this.animated = new Map();
  }
  get(imgName) {
    return new Promise(resolve => {
      if (this.loaded.has(imgName)) {
        resolve(this.loaded.get(imgName).clone());
      } else {
        loadImg(imgName)
          .then(({ mesh }) => {
            mesh.name = imgName;
            this.loaded.set(imgName, mesh);
            mesh = mesh.clone();
            resolve(mesh);
          })
          .catch(() => {
            throw new Error("Could not load " + imgName);
          });
      }
    });
  }
  bulkGet(array) {
    let output = new Map();
    let promiseArray = array.map(elm => {
      return new Promise(resolve => {
        this.get(elm).then(group => {
          output.set(elm, group);
          resolve(group);
        });
      });
    });
    return Promise.all(promiseArray).then(() => {
      output.forEach((elm, key) => {
        output.set(key, elm.clone());
      });
      return output;
    });
  }
  /**
   *
   * @param {String} imgName
   * @param {Number} tilesHoriz The number of tiles in a row
   * @param {Number} tilesVert The number of tiles in a column
   * @param {Number} numTiles The total number of tiles
   * @param {Number} tileDispDuration The duration of a tile in ms
   */
  getAnimated(imgName, tilesHoriz, tilesVert, numTiles, tileDispDuration) {
    return new Promise(resolve => {
      if (this.animated.has(imgName)) {
        resolve(this.animated.get(imgName).mesh.clone());
      } else {
        loadImg(
          imgName,
          true,
          tilesHoriz,
          tilesVert,
          numTiles,
          tileDispDuration
        )
          .then(({ mesh, animatedTexture }) => {
            mesh.name = imgName;
            animatedTexture.mesh = mesh;
            this.animated.set(imgName, animatedTexture);
            mesh = mesh.clone();
            resolve(mesh);
          })
          .catch(() => {
            throw new Error("Could not load " + imgName);
          });
      }
    });
  }
  updateAnimatedTexture(timePos) {
    this.animated.forEach(elm => {
      elm.update(timePos);
    });
  }
}

export const svgManager = new SVGManager();
export const imageManager = new ImageManager();
