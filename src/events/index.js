import { animationManager } from "../three/animation";
import { setupSelectScreen } from "./selectScreen";
window.addEventListener("load", () => {
  setupSelectScreen();
  document.querySelector(".intro .start").addEventListener("click", e => {
    e.preventDefault();
    animationManager.next();
  });
  document.querySelector(".intro2").addEventListener("click", e => {
    e.preventDefault();
    animationManager.next();
  });
  document.querySelector(".map").addEventListener("click", e => {
    e.preventDefault();
    animationManager.selectionScreen();
  });
  document.querySelectorAll(".temple, .gods, .home, .spouse").forEach(elm =>
    elm.addEventListener("click", e => {
      e.preventDefault();
      animationManager.goToMap().then(() => {
        animationManager.selectionScreen();
      });
    })
  );
});
