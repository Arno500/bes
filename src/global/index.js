import debounce from "lodash/debounce";
// import ScrollMagic from "../ext_deps/scrollmagic/ScrollMagic";
// import ScrollMagicAnimation from "../ext_deps/scrollmagic/plugins/animation.gsap";

class RafManager {
  constructor() {
    this.callbacks = [];
  }

  addFunction(func) {
    this.callbacks.push(func);
  }

  start() {
    this.run();
  }

  run() {
    if (this.callbacks.length >= 1) {
      requestAnimationFrame(() => this.run());
      for (let i = 0; i < this.callbacks.length; i++) {
        this.callbacks[i]();
      }
    }
  }
}

class ResizeManager {
  constructor() {
    this.callbacks = [];
    window.onresize = debounce(evt => this.run(evt), 1000 / 60, {
      maxWait: 1000 / 15
    });
  }

  addFunction(func) {
    this.callbacks.push(func);
  }

  run(evt) {
    if (this.callbacks.length >= 1) {
      for (let i = 0; i < this.callbacks.length; i++) {
        this.callbacks[i](evt);
      }
    }
  }
}

class ScrollManager {
  constructor() {
    this.observers = [];
    // this.controller = new ScrollMagic.Controller();
    this.scenes = [];
  }
  addScene(scene) {
    scene.addTo(this.controller);
    this.scenes.push();
  }
  addObserver(elm, func, steps = 1, parent = null, margin = "0px") {
    const options = {
      root: parent,
      rootMargin: margin,
      threshold: this.buildThresholdList(steps)
    };
    const observer = new IntersectionObserver(func, options);
    this.observers.push(observer);
    observer.observe(elm);
  }
  buildThresholdList(numSteps) {
    let thresholds = [];

    for (let i = 1.0; i <= numSteps; i++) {
      let ratio = i / numSteps;
      thresholds.push(ratio);
    }

    thresholds.push(0);
    return thresholds;
  }
}

export const rafManager = new RafManager();
export const resizeManager = new ResizeManager();
export const scrollManager = new ScrollManager();
